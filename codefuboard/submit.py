from flask_wtf import FlaskForm, RecaptchaField
from wtforms.fields import *
from wtforms.validators import DataRequired, Required, Email, StopValidation
from flask_wtf.file import FileRequired
from werkzeug.datastructures import FileStorage
from collections import Iterable

class FilesAllowed(object):
    """Validates that the uploaded file is allowed by a given list of
    extensions or a Flask-Uploads :class:`~flaskext.uploads.UploadSet`.
    This is exclusive for the `MultipleFileField`.
    :param upload_set: A list of extensions or an
        :class:`~flaskext.uploads.UploadSet`
    :param message: error message
    """

    def __init__(self, upload_set, message=None):
        self.upload_set = upload_set
        self.message = message

    def __call__(self, form, field):
        # we need to iterate data field to access filenames
        for dataitem in field.data:
            if not isinstance(dataitem, FileStorage):
                continue

            filename = dataitem.filename.lower()

            if isinstance(self.upload_set, Iterable):
                if any(filename.endswith('.' + x) for x in self.upload_set):
                    continue

                raise StopValidation(self.message or field.gettext(
                    'File does not have an approved extension: {extensions}'
                ).format(extensions=', '.join(self.upload_set)))

            elif not self.upload_set.file_allowed(dataitem, filename):
                raise StopValidation(self.message or field.gettext(
                    'File does not have an approved extension.'
                ))

class SubmitForm(FlaskForm):
    name = TextField(u'Your name', validators=[Required()])
    tname = TextField(u'Your team/group name', validators=[Required()])
    email = TextField(u'Your email address', validators=[Required(),Email()])

    stype = SelectField(u'Program Type', choices=[('cseq', 'C'), ('omp', 'OMP'), ('mpi', 'MPI'), ('openclcpu', 'OpenCL-CPU'), ('openclgpu', 'OpenCL-GPU')])
    source = MultipleFileField (render_kw={'multiple': True}, validators=[DataRequired (), FilesAllowed(['c','h','cl','knl'], 'Only C and OpenCL source files are allowed!')])
    captcha = RecaptchaField()
    submit = SubmitField()

    def validate (self):
        rv = FlaskForm.validate (self)
        if not rv:
            return False

        if self.stype.data in ['cseq','omp','mpi'] and len (self.source.data) > 1:
            self.source.errors.append ("For C, OMP, and MPI you can only pass in 1 file!")
            return False

        return True

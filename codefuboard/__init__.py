from flask import Flask
from flask_appconfig import AppConfig
from flask_bootstrap import Bootstrap
import os

from .base import base
from .db import init_db
from .login import login_manager

def create_app ():
    app = Flask (__name__)

    AppConfig(app)
    Bootstrap (app)

    with app.app_context():
        init_db ()

    app.register_blueprint (base)
    login_manager.init_app(app)

    # create instance path if not there
    if not os.path.isdir (app.instance_path):
        os.mkdir (app.instance_path)
        os.mkdir (os.path.join (app.instance_path, 'data'))
        os.mkdir (os.path.join (app.instance_path, 'errlogs'))

    return app


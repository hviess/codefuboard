"""
Module provides utility functions
"""
import random
import string
import decimal
import re

__search_pattern = re.compile ('Number of iterations: \d+')

def gen_job_id ():
    ''' generate a unique job id '''
    lettersAndDigits = string.ascii_lowercase + string.digits
    return ''.join((random.choice(lettersAndDigits) for i in range(8)))

def get_scale_time (time):
    ''' scale time value (in milliseconds) into either seconds, ms, or even ns depending on the
        size of the number.
    '''
    dtime = decimal.Decimal (time)
    th = decimal.Decimal (1000.0)
    if dtime >= th:
        return (str(dtime / th), 's')
    elif dtime < th and dtime > 0:
        return (str(dtime), 'ms')

def check_output (out):
    return __search_pattern.match (out) != None

def get_iter_output (out):
    if __search_pattern.match (out):
        return out.replace ("Number of iterations: ","")
    else:
        return 'N.A.'

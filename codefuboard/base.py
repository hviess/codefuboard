from flask import Blueprint, render_template, flash, redirect, url_for, current_app, send_from_directory, safe_join, request, session, abort
from werkzeug.utils import secure_filename
from is_safe_url import is_safe_url
import flask_login
from fabric import Connection
from formencode import variabledecode
from apscheduler.schedulers.background import BackgroundScheduler
import atexit
from datetime import datetime
import json
import os
import sys
import io
import shutil

from .submit import SubmitForm
from .login import LoginForm, load_user
from .utils import *
from .job import JobStatus, JobOutputInvalid
from . import db

base = Blueprint ('base', __name__)

# params used to run programs...
params = '10 1000000000'

# ssh connection
ssh = Connection('robotarium-extern', user='codefu')

# cron like service
scheduler = BackgroundScheduler ()

@base.before_app_first_request
def init ():
    global ssh, scheduler
    ssh.open ()
    add_jobs_restart ()
    scheduler.start()
    atexit.register (lambda: scheduler.shutdown ())

@base.context_processor
def get_load_avg():
    '''expose some function to Jinja templating system'''
    return dict(loadavg=os.getloadavg(),
                cleanout=get_iter_output,
                statusgetoptions=JobStatus.getoptions,
                statusnormalise=JobStatus.normalise,
                statusiserror=JobStatus.is_error,
                statusname=JobStatus.get_name)

def add_jobs_restart ():
    global scheduler
    count = 0
    for key in db.jobs_db.keys ():
        entry = db.jobs_db.get (key)
        keyd = key.decode ('utf-8')
        if not JobStatus.is_fail (entry['status']) and JobStatus.DONE.value != JobStatus.normalise (entry['status']):
            current_context = current_app._get_current_object()

            scheduler.add_job (check_job_status, trigger='interval',
                               seconds=10, id=keyd, name='Job {}'.format(keyd),
                               replace_existing=True, args=[current_context, ssh, scheduler, keyd])
            count += 1
    current_app.logger.warning ("Re-added {} jobs...".format (count))

def print_to_log(msg, job_id):
    with open (os.path.join (current_app.instance_path, 'errlogs', job_id), mode='a') as f:
        if isinstance (msg, str):
            print ('[{}] {}'.format(datetime.utcnow().isoformat(), msg), file=f)
        elif isinstance (msg, io.StringIO):
            f.write (msg.getvalue ())

def delete_job (job_id, delmetric=False):
    """
    remove a job and its data on the filesystem
    """
    global scheduler
    if scheduler.get_job (job_id):
        scheduler.remove_job (job_id)

    entry = db.jobs_db.get (job_id)
    if entry:
        db.jobs_db.rem (job_id)

    if not delmetric:
        mentry = db.metrics_db.get (job_id)
        if mentry and 'sources' not in mentry:
            if 'src' in entry and entry['src']:
                mentry['sources'] = {entry['src-name']: entry['src']}
            else:
                mentry['sources'] = entry['sources']

def delete_metric (job_id):
    """
    remove the metric, its associated job, and all data on the filesystem
    """
    delete_job (job_id, delmetric=True)
    shutil.rmtree (os.path.join (current_app.instance_path, job_id), ignore_errors=True)
    errlog = os.path.join (current_app.instance_path, 'errlogs', job_id)
    if os.path.isfile (errlog):
        os.remove (errlog)
    db.metrics_db.rem (job_id)

def send_job (job_id, job_type, sources, rerun=False):
    global ssh, params
    if rerun:
        ssh.run ('rm -rf codefu/jobs/{}'.format(job_id), warn=True, hide='both')
    if ssh.run ('mkdir -p codefu/jobs/{}'.format (job_id)).ok:
        for name, loc in sources.items():
            path = os.path.join('codefu/jobs/', job_id, name)
            ssh.put (loc, remote=path)
        ssh.run ('touch codefu/jobs/{}/.type-{}'.format (job_id, job_type))
        ssh.run ('touch codefu/jobs/{}/.SUBMITTED'.format (job_id))
        if job_type == 'openclgpu':
            ssh.run ('echo "10 200000000" > codefu/jobs/{}/.params'.format (job_id))
        else:
            ssh.run ('echo "{}" > codefu/jobs/{}/.params'.format (params, job_id))
    else:
        current_app.logger.error ("unable to create directory on remote!")

def check_job_status (app, ssh, watcher, job_id):
    with app.app_context():
        entry = db.jobs_db.get(job_id)

        if not JobStatus.is_fail (entry['status']):
            # check for error state
            if ssh.run ('test -f codefu/jobs/{}/.ERROR'.format (job_id), warn=True, hide=True).ok:
                entry['status'] = JobStatus.ERROR.value
                db.jobs_db.set (job_id, entry)

            # check if we timedout
            elif ssh.run ('test -f codefu/jobs/{}/.TIMEOUT'.format (job_id), warn=True, hide=True).ok:
                app.logger.warn ('job {} timed out!'.format(job_id))
                entry['status'] = JobStatus.TIMEOUT.value
                db.jobs_db.set (job_id, entry)
                watcher.remove_job(job_id)

        if entry['status'] == JobStatus.DONE.value:
            # metrics db fields:
            # - runtime, unit, raw_runtime, hear, array_size, out, date, team

            fmetrics = io.StringIO()
            if ssh.run ('cat codefu/metrics/{}.json'.format(job_id), warn=True, hide='out', out_stream=fmetrics).ok:
                app.logger.info ('job {} complete, getting metrics and removing poll...'.format(job_id))
                # if we are unable to parse the data, we change status to error, and mark this in the log.
                try:
                    metrics = json.loads (fmetrics.getvalue())[0]
                    if not check_output (metrics['out']):
                        raise JobOutputInvalid (metrics['out'])
                    metrics['runtime'], metrics['unit'] = get_scale_time (metrics['raw_runtime'])
                    metrics['date'] = entry['date']
                    metrics['team'] = entry['team']
                    metrics['type'] = entry['type']
                    if entry['src']:
                        metrics['sources'] = {entry['src-name']: entry['src']}
                    else:
                        metrics['sources'] = entry['sources']
                    metrics['checked'] = False
                    db.metrics_db.set (job_id, metrics)
                except JobOutputInvalid as e:
                    app.logger.error ('job {} output does not match regex "Number of iterations: \d+", marking job as invalid!'.format(job_id))
                    entry['status'] = JobStatus.INVALID.value
                    db.jobs_db.set (job_id, entry)
                    print_to_log ('output does not match regex "Number of iterations: \d+"! See:', job_id)
                    print_to_log (e.args[0], job_id)
                except json.decoder.JSONDecodeError:
                    app.logger.error ('job {} output is mangeled, marking job as fail!'.format(job_id))
                    entry['status'] = JobStatus.INVALID.value
                    db.jobs_db.set (job_id, entry)
                    print_to_log ('Unable to parse returned data, it looks to be mangled! See:', job_id)
                    print_to_log (fmetrics, job_id)
                watcher.remove_job(job_id)
            else:
                app.logger.info ('job {} missing metric, will try again...'.format(job_id))

        elif entry['status'] == JobStatus.SUBMITTED.value:
            if ssh.run ('test -f codefu/jobs/{}/.RUNNING'.format (job_id), warn=True, hide=True).ok:
                entry['status'] = JobStatus.RUNNING.value
                db.jobs_db.set (job_id, entry)
        elif entry['status'] == JobStatus.ERROR.value:
            flog = io.StringIO()
            if ssh.run ('test -e codefu/jobs/{}/log'.format(job_id), warn=True, hide='both').ok:
                app.logger.warn ('job {} errored, see log for details...'.format(job_id))
                ssh.get ('codefu/jobs/{}/log'.format (job_id), os.path.join (app.instance_path, 'errlogs', job_id))
                watcher.remove_job(job_id)
            else:
                app.logger.info ('job {} errored, but gave no log, will try again...'.format(job_id))
        elif entry['status'] == JobStatus.RUNNING.value:
            if ssh.run ('test -f codefu/jobs/{}/.DONE'.format (job_id), warn=True, hide=True).ok:
                entry['status'] = JobStatus.DONE.value
                db.jobs_db.set (job_id, entry)

@base.route ('/')
def index ():
    return render_template ('index.html')

@base.route ('/submit/', methods=('GET', 'POST'))
def submit ():
    global scheduler
    form = SubmitForm ()
    if form.validate_on_submit():
        job_id = gen_job_id()

        # create data dir
        os.makedirs (os.path.join(current_app.instance_path, 'data', job_id))

        # get and save files in data dir
        f = form.source.data
        sources = {}
        for fl in f:
            fname = secure_filename(fl.filename)
            floc = os.path.join(current_app.instance_path, 'data', job_id, fname)
            sources[str(fname)] = str(floc)
            fl.save(floc)

        # job db fields
        # - date, name, email, team, type, src-name, src, status

        entry = {'date': datetime.utcnow().isoformat(),
                 'name': form.name.data,
                 'email': form.email.data,
                 'team': form.tname.data,
                 'type': form.stype.data,
                 'sources': sources,
                 'src-name': None,
                 'src': None,
                 'status': JobStatus.SUBMITTED.value}

        # send the job
        send_job (job_id, form.stype.data, sources)
        db.jobs_db.set (job_id, entry)

        current_context = current_app._get_current_object()

        scheduler.add_job (check_job_status, trigger='interval',
                           seconds=10, id=job_id, name='Job {}'.format(job_id),
                           replace_existing=True, args=[current_context, ssh, scheduler, job_id])

        context = {'submit_id': job_id, 'fname': sources.keys ()}
        return render_template ('submit-ok.html', **context)
    return render_template ('submit.html', form=form)

@base.route ('/admin/login/', methods=['GET', 'POST'])
def admin_login ():
    form = LoginForm ()
    if form.validate_on_submit ():
        user = load_user (form.user.data)
        if not user:
            return abort(400)
        flask_login.login_user (user)
        flash('Logged in successfully.', 'success')

        next = request.args.get('next')
        if not is_safe_url(next, {'127.0.0.1', 'lb.oi.pe'}):
            return abort(400)

        return redirect (next or url_for ('base.admin'))
    return render_template ('admin/login.html', form=form)

@base.route("/admin/logout/")
@flask_login.login_required
def admin_logout ():
    flask_login.logout_user ()
    return redirect (url_for ('base.admin'))

@base.route ('/admin/')
@flask_login.login_required
def admin ():
    return render_template ('admin/index.html')

@base.route ('/admin/jobs/')
@flask_login.login_required
def admin_jobs ():
    global scheduler
    jobs = scheduler.get_jobs ()
    num_jobs = len (jobs)
    if 'admin-flash' in session:
        flash (session['admin-flash'][0], session['admin-flash'][1])
        del session['admin-flash']
        session.modified = True
    return render_template ('admin/jobs.html', db=db.jobs_db, njobs=num_jobs, bjobs=jobs)

@base.route ('/admin/jobs/src/<job_id>')
@flask_login.login_required
def admin_job_src (job_id):
    jentry = db.jobs_db.get (job_id)
    mentry = db.metrics_db.get (job_id)
    try:
        content = {}
        if jentry:
            if 'src' in jentry and jentry['src']:
                with open (jentry['src'], 'r') as f:
                    content[jentry['src-name']] = f.read()
            elif 'sources' in jentry and jentry['sources']:
                for name, src in jentry['sources'].items():
                    with open (src, 'r') as f:
                        content[name] = f.read ()
        elif mentry:
            if 'src' in mentry and mentry['src']:
                with open (mentry['src'], 'r') as f:
                    content[mentry['src-name']] = f.read()
            elif 'sources' in mentry and mentry['sources']:
                for name, src in mentry['sources'].items():
                    with open (src, 'r') as f:
                        content[name] = f.read ()
        else:
            raise FileNotFoundError ("Unable to find source file(s), not in job or metrics DB!")
        return render_template('admin/src.html', job=job_id, sources=content)
    except (FileNotFoundError, TypeError):
        return render_template('admin/src-not-found.html', job=job_id), 404

@base.route ('/admin/jobs/<action>/<job_id>/', methods=['POST'])
@flask_login.login_required
def admin_job_modify (action, job_id):
    global scheduler, ssh
    if action == 'delete':
        delete_job (job_id)
        session['admin-flash'] = ['Deleted job {}...'.format (job_id), 'danger']
        session.modified = True
    elif action == 'save':
        postvars = variabledecode.variable_decode(request.form, dict_char='_')
        entry = db.jobs_db.get (job_id)
        values = postvars.get (job_id)
        # we need to make sure status is stored as int
        values['status'] = int(values['status'])
        entry.update (values)
        db.jobs_db.set (job_id, entry)
        session['admin-flash'] = ['Saved job {}...'.format (job_id), 'success']
        session.modified = True
    elif action == 'rerun':
        entry = db.jobs_db.get (job_id)
        if scheduler.get_job (job_id):
            scheduler.remove_job (job_id)
        if 'src' in entry and entry['src']:
            sources = {entry['src-name'], entry['src']}
            send_job (job_id, entry['type'], sources, rerun=True)
        else:
            send_job (job_id, entry['type'], entry['sources'], rerun=True)
        entry['status'] = JobStatus.SUBMITTED.value
        db.jobs_db.set (job_id, entry)
        current_context = current_app._get_current_object()
        if scheduler.get_job (job_id):
            scheduler.remove_job (job_id)
        scheduler.add_job (check_job_status, trigger='interval',
                           seconds=10, id=job_id, name='Job {}'.format(job_id),
                           replace_existing=True, args=[current_context, ssh, scheduler, job_id])
        session['admin-flash'] = ['Re-running job {}...'.format (job_id), 'primary']
        session.modified = True
    elif action == 'rewatch':
        entry = db.jobs_db.get (job_id)
        entry['status'] = JobStatus.RUNNING.value
        db.jobs_db.set (job_id, entry)
        current_context = current_app._get_current_object()
        if scheduler.get_job (job_id):
            scheduler.remove_job (job_id)
        scheduler.add_job (check_job_status, trigger='interval',
                           seconds=10, id=job_id, name='Job {}'.format(job_id),
                           replace_existing=True, args=[current_context, ssh, scheduler, job_id])
        session['admin-flash'] = ['Re-watching job {}...'.format (job_id), 'warning']
        session.modified = True

    return redirect (url_for ('base.admin_jobs'))

@base.route ('/admin/metrics/')
@flask_login.login_required
def admin_metrics ():
    if 'admin-flash' in session:
        flash (session['admin-flash'][0], session['admin-flash'][1])
        del session['admin-flash']
        session.modified = True
    return render_template ('admin/metrics.html', db=db.metrics_db, dbj=db.jobs_db)

@base.route ('/admin/metrics/<action>/<job_id>/', methods=['POST'])
@flask_login.login_required
def admin_metrics_modify (action, job_id):
    if action == 'delete':
        delete_metric (job_id)
        session['admin-flash'] = ['Deleted metrics for job {}...'.format (job_id), 'danger']
        session.modified = True
    elif action == 'check':
        entry = db.metrics_db.get (job_id)
        msg = ''
        if 'checked' in entry and entry['checked']:
            entry['checked'] = False
            msg = "UNCHECKED"
        else:
            entry['checked'] = True
            msg = "CHECKED"
        db.metrics_db.set (job_id, entry)
        session['admin-flash'] = ['Marked metric for job {} as {}'.format (job_id, msg), 'success']
        session.modified = True

    return redirect (url_for ('base.admin_metrics'))

@base.route ('/lb/complete/')
def lb ():
    return render_template ('leaderboard.html', db=db.metrics_db)

@base.route ('/lb/')
def lbsort ():
    out = {'c': {}, 'omp': {}, 'mpi': {}, 'openclcpu': {}, 'openclgpu': {}}
    for key in db.metrics_db.keys():
        entry = db.metrics_db.get (key)
        if entry['type'] == 'cseq':
            if entry['team'] not in out['c'] or out['c'][entry['team']]['raw_runtime'] > entry['raw_runtime']:
                out['c'][entry['team']] = entry
                out['c'][entry['team']]['job_id'] = key.decode ('utf-8')
        elif entry['type'] == 'omp':
            if entry['team'] not in out['omp'] or out['omp'][entry['team']]['raw_runtime'] > entry['raw_runtime']:
                out['omp'][entry['team']] = entry
                out['omp'][entry['team']]['job_id'] = key.decode ('utf-8')
        elif entry['type'] == 'mpi':
            if entry['team'] not in out['mpi'] or out['mpi'][entry['team']]['raw_runtime'] > entry['raw_runtime']:
                out['mpi'][entry['team']] = entry
                out['mpi'][entry['team']]['job_id'] = key.decode ('utf-8')
        elif entry['type'] == 'openclcpu':
            if entry['team'] not in out['openclcpu'] or out['openclcpu'][entry['team']]['raw_runtime'] > entry['raw_runtime']:
                out['openclcpu'][entry['team']] = entry
                out['openclcpu'][entry['team']]['job_id'] = key.decode ('utf-8')
        elif entry['type'] == 'openclgpu':
            if entry['team'] not in out['openclgpu'] or out['openclgpu'][entry['team']]['raw_runtime'] > entry['raw_runtime']:
                out['openclgpu'][entry['team']] = entry
                out['openclgpu'][entry['team']]['job_id'] = key.decode ('utf-8')
    return render_template ('leaderboard-sort.html', c=out['c'].values(), omp=out['omp'].values(), mpi=out['mpi'].values(), openclcpu=out['openclcpu'].values (), openclgpu=out['openclgpu'].values ())

@base.route ('/jobs/')
def jobs ():
    global scheduler
    jobs = scheduler.get_jobs ()
    num_jobs = len (jobs)
    return render_template ('jobs.html', db=db.jobs_db, njobs=num_jobs, bjobs=jobs)

@base.route ('/jobs/log/<string:job_id>')
def job_log (job_id):
    try:
        fpath = safe_join (os.path.join (current_app.instance_path, 'errlogs'), job_id)
        content = ''
        with open (fpath, 'r') as f:
            content = f.read()
        return render_template('log.html', job=job_id, log=content)
    except FileNotFoundError:
        return render_template('log-not-found.html', job=job_id), 404

@base.errorhandler (404)
def page_not_found (error):
    return render_template('404.html'), 404

@base.route('/robots.txt')
def static_from_root():
    return send_from_directory(current_app.static_folder, request.path[1:])

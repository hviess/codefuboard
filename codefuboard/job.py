from enum import Enum

class JobOutputInvalid (ValueError):
    pass

class JobOutputMangled (ValueError):
    pass

class JobStatus (Enum):
    # job has been submitted to cluster
    SUBMITTED = 1
    # job is running on cluster
    RUNNING = 2
    # job finished with exit status 0
    DONE = 3
    # job on cluster timed out
    TIMEOUT = 4
    # job finished with exit status != 0
    ERROR = 5
    # job result is invalid
    INVALID = 6

    def __str__ (self):
        return str(self.value)

    @staticmethod
    def getoptions (status) -> str:
        status_val = JobStatus.normalise (status)
        selection = ''
        for name, e in JobStatus.__members__.items ():
            if status_val == e.value:
                selection+='<option value="{}" selected>{}</option>'.format (e.value, name)
            else:
                selection+='<option value="{}">{}</option>'.format (e.value, name)
        return selection

    @staticmethod
    def is_error (status) -> bool:
        if isinstance (status, int):
            return JobStatus (status).value > JobStatus.TIMEOUT.value
        elif isinstance (status, str):
            if status in JobStatus.__members__:
                return JobStatus.__members__[status].value > JobStatus.TIMEOUT.value
            else:
                raise KeyError("Status name {} does not exist!".format(status))

    @staticmethod
    def is_fail (status) -> bool:
        if isinstance (status, int):
            return status > JobStatus.DONE.value
        elif isinstance (status, str):
            if status in JobStatus.__members__:
                return JobStatus.__members__[status].value > JobStatus.DONE.value
            else:
                raise KeyError("Status name {} does not exist!".format(status))

    @staticmethod
    def get_name (status) -> str:
        if isinstance (status, int):
            return JobStatus (status).name
        elif isinstance (status, str):
            if status in JobStatus.__members__:
                return status
            else:
                raise KeyError("Status name {} does not exist!".format(status))

    @staticmethod
    def normalise (status) -> int:
        if isinstance (status, int):
            return status
        elif isinstance (status, str):
            if status in JobStatus.__members__:
                return JobStatus.__members__[status].value
            else:
                raise KeyError("Status name {} does not exist!".format(status))

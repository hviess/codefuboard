from flask import current_app
import pickledb
import redis
import msgpack
import os

# import data from pickledb based databases into redis
IMPORT = False

admin_db = None
jobs_db = None
metrics_db = None

class CodeFuDatabase ():
    def __init__ (self, name, redisdb, autosave=False):
        self.db = redisdb
        self.name = name
        self.autosave = autosave

    def len (self):
        return self.db.dbsize ()

    def get (self, key):
        v = self.db.get (key)
        if v:
            d = msgpack.unpackb (v)
            return d
        else:
            return v

    def set (self, key, value):
        d = msgpack.packb (value)
        self.db.set (key, d)
        self.__autosave ()

    def rem (self, key):
        self.db.delete (key)
        self.__autosave ()

    def keys (self):
        return self.db.scan_iter ()

    def dump (self):
        self.db.bgsave ()

    def __autosave (self):
        if self.autosave:
            self.dump ()

def init_db ():
    global jobs_db, admin_db, metrics_db
    # we need to collect the data from existing pickledb files, iff they exist
    # we must set db in connection pool due to strange internal design: https://github.com/andymccurdy/redis-py/issues/235
    jobs_db = CodeFuDatabase ('jobs', redis.Redis (connection_pool=redis.BlockingConnectionPool(db=5)))
    metrics_db = CodeFuDatabase ('metrics', redis.Redis (connection_pool=redis.BlockingConnectionPool(db=4)))
    admin_db = CodeFuDatabase ('admin', redis.Redis (connection_pool=redis.BlockingConnectionPool(db=3)))

    if IMPORT:
        if os.path.exists (os.path.join(current_app.instance_path, 'jobs.db')):
            db = pickledb.load(os.path.join(current_app.instance_path, 'jobs.db'), False, sig=False)
            for key in db.getall ():
                jobs_db.set (key, db.get (key))

            db.deldb ()
            db.dump ()
        if os.path.exists (os.path.join(current_app.instance_path, 'metrics.db')):
            db = pickledb.load(os.path.join(current_app.instance_path, 'metrics.db'), False, sig=False)
            for key in db.getall ():
                metrics_db.set (key, db.get (key))

            db.deldb ()
            db.dump ()
        if os.path.exists (os.path.join(current_app.instance_path, 'admin.db')):
            db = pickledb.load(os.path.join(current_app.instance_path, 'admin.db'), False, sig=False)
            for key in db.getall ():
                admin_db.set (key, db.get (key))

            db.deldb ()
            db.dump ()

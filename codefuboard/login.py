from flask_wtf import FlaskForm, RecaptchaField
from wtforms.fields import TextField, PasswordField, SubmitField
from wtforms.validators import Required
from flask import current_app
from werkzeug.security import check_password_hash
import flask_login

from . import db

login_manager = flask_login.LoginManager()
login_manager.login_view = 'base.admin_login'

class LoginForm (FlaskForm):
    user = TextField (u'Username', validators=[Required()])
    passwd = PasswordField (u'Password', validators=[Required()])

    captcha = RecaptchaField()
    submit = SubmitField()

class AdminUser (flask_login.UserMixin):
    pass

@login_manager.user_loader
def load_user (user_id):
    if not db.admin_db.get (user_id):
        return None

    user = AdminUser ()
    user.id = user_id
    return user

@login_manager.request_loader
def request_loader(request):
    user_id = request.form.get ('user')
    if not user_id or not db.admin_db.get (user_id):
        return None

    user = AdminUser ()
    user.id = user_id

    passwd = db.admin_db.get(user_id)['passwd']

    user.is_authenticated = check_password_hash (passwd, request.form['passwd'])
    return user

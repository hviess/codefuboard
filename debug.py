from codefuboard import *
from flask_socketio import SocketIO

app = create_app()
socketio = SocketIO (app)

if __name__ == '__main__':
    socketio.run(app, debug=True)

#!/usr/bin/env bash

# compile and run OpenCL C code on GPU
# we assume that sbatch is called with `-D dir/to/job/source/code`

#SBATCH --exclusive
#SBATCH --gres=gpu:k20:1
#SBATCH --partition amd-shortq
#SBATCH --time 25
#SBATCH --mem 0
#SBATCH --signal=B:SIGINT@30
#SBATCH --output=log
#SBATCH --open-mode=append

# we need to manually include the module command
source /etc/profile.d/modules.sh

module load gcc
module load cuda10.1/toolkit

# timer, in millisecond resolution
function time-ms() {
    local ts
    local tt
    local e
    local pid
    ts=$(date +%s%N)
    "$@" &
    pid=$!
    wait $pid # we use this to handle signals
    e=$?
    tt=$((($(date +%s%N) - ts)/1000000))
    echo -n "$tt" >&3
    return $e
}

function tolog() {
    echo "[$(date -u -R)]: $*" >&2
}

function sig_handler() {
    touch .TIMEOUT
    tolog "SLURM killed process due to timeout condition"
    exit 2
}

# if we hit slurms timeout (or other limits), we want to indicate this...
trap 'sig_handler' INT TERM

declare -a SRCS
for f in *.c; do
    SRCS+=("$(basename "$f")");
done

JID="$1"
HEAT=$(cut -d' ' -f1 .params)
ARRAY=$(cut -d' ' -f2 .params)

tolog "running compiler"
tolog "compiling the following C files: ${SRCS[*]}"
if gcc -std=c99 -D_GNU_SOURCE -O3 -Wall -march=native -I. -I/cm/shared/apps/cuda10.1/toolkit/current/include -L/cm/shared/apps/cuda10.1/toolkit/current/lib64 -Wl,-rpath=/cm/shared/apps/cuda10.1/toolkit/current/lib64 -lOpenCL -lrt -o program.out "${SRCS[@]}" ; then
    echo -n "[{ \"slurm_node\": \"$SLURM_JOB_NODELIST\" , \"heat\": $HEAT, \"array_size\": $ARRAY, \"raw_runtime\": " > /home/codefu/codefu/metrics/"$JID".json
    tolog "running program"
    if ! time-ms ./program.out "$HEAT" "$ARRAY" 3>>/home/codefu/codefu/metrics/"$JID".json >.out; then
        tolog "program encountered an error, exiting..."
        touch .ERROR
        exit 1
    fi
    OUT=$(<.out)
    echo -n ", \"out\": \"$OUT\"}]" >> /home/codefu/codefu/metrics/"$JID".json
    tolog "program completed successfully"
    touch .DONE
    exit 0
else
    tolog "unable to compiler program"
    touch .ERROR
    exit 1
fi

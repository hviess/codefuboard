#!/usr/bin/env bash

SBATCH="/cm/shared/apps/slurm/17.11.12/bin/sbatch"

inotifywait -m /home/codefu/codefu/jobs -e create -e moved_to |
    while read path action file; do
		echo "detected change ($action) in $path$file..."
		sleep 5 # really?!
		if [ -e "$path$file/.type-cseq" ]; then
			echo "launching sbatch for C-seq"
			if $SBATCH -D "$path$file" -J "codefu c $file" /home/codefu/git/codefuboard/scripts/run-c-opt.sh "$file"; then
				touch "$path$file/.RUNNING"
			else
				echo "Unable to launch SBATCH script!" >> "${path}${file}/log"
				touch "$path$file/.ERROR"
			fi
		elif [ -e "$path$file/.type-omp" ]; then
			echo "launching sbatch for OMP"
			if $SBATCH -D "$path$file" -J "codefu omp $file" /home/codefu/git/codefuboard/scripts/run-openmp.sh "$file"; then
				touch "$path$file/.RUNNING"
			else
				echo "Unable to launch SBATCH script!" >> "${path}${file}/log"
				touch "$path$file/.ERROR"
			fi
		elif [ -e "$path$file/.type-mpi" ]; then
			echo "launching sbatch for MPI"
			if $SBATCH -D "$path$file" -J "codefu mpi $file" /home/codefu/git/codefuboard/scripts/run-mpi.sh "$file"; then
				touch "$path$file/.RUNNING"
			else
				echo "Unable to launch SBATCH script!" >> "${path}${file}/log"
				touch "$path$file/.ERROR"
			fi
		elif [ -e "$path$file/.type-openclcpu" ]; then
			echo "launching sbatch for OpenCL CPU"
			if $SBATCH -D "$path$file" -J "codefu clcpu $file" /home/codefu/git/codefuboard/scripts/run-opencl-cpu.sh "$file"; then
				touch "$path$file/.RUNNING"
			else
				echo "Unable to launch SBATCH script!" >> "${path}${file}/log"
				touch "$path$file/.ERROR"
			fi
		elif [ -e "$path$file/.type-openclgpu" ]; then
			echo "launching sbatch for OpenCL GPU"
			if $SBATCH -D "$path$file" -J "codefu clgpu $file" /home/codefu/git/codefuboard/scripts/run-opencl-gpu.sh "$file"; then
				touch "$path$file/.RUNNING"
			else
				echo "Unable to launch SBATCH script!" >> "${path}${file}/log"
				touch "$path$file/.ERROR"
			fi
		fi
    done

#!/usr/bin/env bash
function time-ms() {
    local ts
    local tt
    ts=$(date +%s%N) ; "$@" ; tt=$((($(date +%s%N) - ts)/1000000)) ; echo "[{ \"runtime\": $tt }]"
}

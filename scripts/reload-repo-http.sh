#!/usr/bin/env bash

# this scripts performs a git pull and restarts the uwsgi service
# NOTE: it is assumed that this app is run by root!

# it might be a good idea to setup git credential store, so that we
# don't need to hardcode passwords anywhere.

GITREPODIR="$1"
USER="$2"
SERVICE="lb" # we assume...

if ! sudo -u "$USER" git -C "$GITREPODIR" pull --quiet; then
    echo "Was unable to perform pull! Git returned $?."
    exit $?
else
    # perform service restart only after pull!
    if ! systemctl --quiet restart uwsgi@"$SERVICE".service; then
        echo "Was unable to restart uwsgi service! Systemctl returned $?."
        exit $?
    fi
fi


#!/usr/bin/env bash

# compile and run C code + OpenMP
# we assume that sbatch is called with `-D dir/to/job/source/code`

#SBATCH -n 128
#SBATCH --partition amd-longq
#SBATCH --time 25
#SBATCH --mem 0
#SBATCH --signal=B:SIGINT@30
#SBATCH --output=log
#SBATCH --open-mode=append

# we need to manually include the module command
source /etc/profile.d/modules.sh

module load slurm
module load gcc
module load mpich

# timer, in millisecond resolution
function time-ms() {
    local ts
    local tt
    local e
    local pid
    ts=$(date +%s%N)
    "$@" &
    pid=$!
    wait $pid # we use this to handle signals
    e=$?
    tt=$((($(date +%s%N) - ts)/1000000))
    echo -n "$tt" >&3
    return $e
}

function tolog() {
    echo "[$(date -u -R)]: $*" >&2
}

function sig_handler() {
    touch .TIMEOUT
    tolog "SLURM killed process due to timeout condition"
    exit 2
}

# if we hit slurms timeout (or other limits), we want to indicate this...
trap 'sig_handler' INT TERM

JID="$1"
SRC=$(basename -- *.c)
HEAT=$(cut -d' ' -f1 .params)
ARRAY=$(cut -d' ' -f2 .params)

tolog "running compiler"
if mpicc -std=c99 -O3 -Wall -march=native "$SRC" -o program.out; then
    echo -n "[{ \"slurm_node\": \"$SLURM_JOB_NODELIST\" , \"heat\": $HEAT, \"array_size\": $ARRAY, \"raw_runtime\": " > /home/codefu/codefu/metrics/"$JID".json
    tolog "running program"
    if ! time-ms srun --mpi=pmi2 ./program.out "$HEAT" "$ARRAY" 3>>/home/codefu/codefu/metrics/"$JID".json >.out; then
        tolog "program encountered an error, exiting..."
        touch .ERROR
        exit 1
    fi
    OUT=$(<.out)
    echo -n ", \"out\": \"$OUT\"}]" >> /home/codefu/codefu/metrics/"$JID".json
    tolog "program completed successfully"
    touch .DONE
    exit 0
else
    tolog "unable to compiler program"
    touch .ERROR
    exit 1
fi
